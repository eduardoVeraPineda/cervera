table_index = $('#tableindex').DataTable({
  language: {
    url: "//cdn.datatables.net/plug-ins/1.10.13/i18n/Spanish.json"
  },
  scrollY: (window.innerHeight - 274) + "px",
  scrollCollapse: false,
  dom: 'Bfrtip',
  buttons: ['excel', 'pdf',],
  initComplete: function(settings, json) {
    $('.dataTables_filter').before(
    );
    $('table').css("opacity", "1");
    $('#loader').css("opacity", "0");
  }
});
