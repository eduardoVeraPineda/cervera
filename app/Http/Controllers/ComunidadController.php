<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Comunidad;

class ComunidadController extends Controller
{

    public function index()
    {
        $comunidades = Comunidad::orderBy('nombre','ASC')->paginate();
        return view('admin.comunidades.index')->with('comunidades',$comunidades);
    }

    public function list()
    {
        return response()->json(Comunidad::all(), 201);
    }

    public function create()
    {
        return view('admin.comunidades.create');
    }

    public function store(Request $request)
    {
        $comunidad = Comunidad::create($request->all());
        return response()->json($comunidad, 201);
    }

    public function edit($id)
    {
      $comunidad = Comunidad::find($id);
      return view('admin.comunidades.edit')->with('comunidad',$comunidad);
    }

    public function show($id)
    {
        return response()->json(Comunidad::find($id), 201);
    }

    public function update(Request $request, $id)
    {
        $comunidad = Comunidad::find($id);
        $comunidad->update($request->all());
        return response()->json($comunidad, 201);
    }

    public function destroy($id)
    {
        $comunidad = Comunidad::find($id);
        $comunidad->delete();
        return response()->json($comunidad, 201);
    }
}
