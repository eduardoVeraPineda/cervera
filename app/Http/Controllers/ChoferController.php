<?php

namespace App\Http\Controllers;

use App\Chofer;
use Illuminate\Http\Request;

class ChoferController extends Controller
{
    public function index()
    {
        $choferes = Chofer::all();
        return view('admin.choferes.index')->with('choferes',$choferes);
    }

    public function list()
    {
        return response()->json(Chofer::all(), 201);
    }

    public function create()
    {
      return view('admin.choferes.create');
    }

    public function store(Request $request)
    {
        $chofer = Chofer::create($request->all());
        return response()->json($chofer, 201);
    }

    public function edit($id)
    {
        $chofer = Chofer::find($id);
        return view('admin.choferes.edit')->with('chofer',$chofer);
    }

    public function show($id)
    {
        return response()->json(Chofer::find($id), 201);
    }

    public function update(Request $request,$id)
    {
        $chofer = Chofer::find($id);
        $chofer->update($request->all());
        return response()->json($chofer, 201);
    }

    public function destroy($id)
    {
        $chofer = Chofer::find($id);
        $chofer->delete();
        return response()->json($chofer, 201);
    }
}
