<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Paquete;
use App\Comunidad;

class PaqueteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $paquetes = Paquete::all();

        return view('admin.paquetes.index')->with('paquetes',$paquetes);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function list()
    {
        return response()->json(Paquete::all(), 201);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
      $comunidades = Comunidad::orderBy('nombre','ASC')->pluck('nombre','id');
      return view('admin.paquetes.create')->with('comunidades',$comunidades);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $paquete = Paquete::create($request->all());
        return response()->json($paquete, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return response()->json(Paquete::find($id), 201);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request,$id)
    {
        $paquete = Paquete::find($id);
        $paquete->update($request->all());
        return response()->json($paquete, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
