<?php

namespace App\Http\Controllers;

use App\Cliente;
use Illuminate\Http\Request;

class ClienteController extends Controller
{
    public function index()
    {
        $clientes = Cliente::all();
        return view('admin.clientesfavs.index')->with('clientes',$clientes);

    }
    public function list()
    {
        return response()->json(Cliente::all(), 201);

    }

    public function create()
    {
      return view('admin.clientesfavs.create');
    }

    public function store(Request $request)
    {
        $cliente = Cliente::create($request->all());
        return response()->json($cliente, 201);
    }

    public function show($id)
    {
        return response()->json(Cliente::find($id), 201);
    }

    public function edit($id)
    {
        $cliente = Cliente::find($id);
        $cliente->comunidad;
        return view('admin.clientesfavs.edit')->with('cliente',$cliente);
    }

    public function update(Request $request,$id)
    {
        $cliente = Cliente::find($id);
        $cliente->update($request->all());
        return response()->json($cliente, 201);
    }

    public function destroy($id)
    {
        //
    }
}
