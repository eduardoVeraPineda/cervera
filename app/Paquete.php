<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paquete extends Model
{
    //
    protected $table = "paquetes";

    protected $fillable = [
      'nombre',
      'apPaterno',
      'apMaterno',
      'telefono',
      'calle',
      'pago',
      'monto',
      'pesoTotal',
      'observaciones',
      'comunidad_id'
    ];

    public function comunidades(){
      return $this->belongsTo('App\Comunidad','comunidad');
    }

    public function piezas(){
      return $this->hasMany('App\Pieza');
    }
}
