<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    //
    protected $table = "clientesfavs";

    protected $fillable = [
      'nombre',
      'apPaterno',
      'apMaterno',
      'telefono',
      'calle',
      'comunidad_id'
    ];

    public function comunidad(){
      return $this->belongsTo('App\Comunidad','comunidad_id');
    }
}
