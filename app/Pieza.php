<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pieza extends Model
{
    //
    protected $table = "piezas";

    protected $fillable = [
      'piezaID',
      'tipo',
      'peso',
      'paquete'
    ];

    public function paquetes(){
      return $this->belongsTo('App\Paquete','paquete');
    }
}
