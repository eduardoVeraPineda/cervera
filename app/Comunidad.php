<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comunidad extends Model
{
    //
    protected $table = "comunidades";

    protected $fillable = [
      'nombre',
      'estado',
      'municipio'
    ];

    public function paquetes(){
      return $this->hasMany('App\Paquete');
    }

    public function clientes(){
      return $this->hasMany('App\Cliente');
    }
}
