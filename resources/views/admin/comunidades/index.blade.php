@extends('admin.templates.layout')
@section('css')

@endsection
@section('title','Cervera')

@section('content-fieldset')

	<div class="row">
		<div class="col s9">
			<caption><h4>Comunidades</h4></caption>
		</div>
		<div class="col s3 right-align valign-wrapper">
			<h4>
				<a href="{{ route('comunidades.create') }}" class="btn">+ Agregar</a>
			</h4>
		</div>
	</div>
  <fieldset id="fieldset-content" class="col s12">
    <table id="tableindex">
  		<thead>
  			<tr>
  				<th>Nombre</th>
          <th>Municipio</th>
  				<th>Estado</th>
  				<th>Acción</th>
  			</tr>
  		</thead>
  		<tbody>
  			@foreach($comunidades as $comunidad)
  			<tr>
  				<td>{{ $comunidad->nombre }}</td>
          <td>{{ $comunidad->municipio }}</td>
  				<td>{{ $comunidad->estado }}</td>
  				<td>
  					<a href="{{ route('comunidades.edit', $comunidad->id) }}" class="btn orange lighten-2"><i class="material-icons">mode_edit</i></a>
  					<a href="{{ route('comunidades.destroy', $comunidad->id) }}" class="btn red lighten-1"><i class="material-icons">delete</i></a>
  				</td>
  			</tr>
  			@endforeach
  		</tbody>
  	</table>
  </fieldset>

@endsection
