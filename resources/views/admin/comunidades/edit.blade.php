@extends('admin.templates.layout')
@section('css')

@endsection
@section('title','Crear')

@section('content-fieldset')

	<h4>Comunidad</h4>
	<fieldset class="col s12 m12" style="padding: 20px 0px; background-color:white">
		<h6 style="margin: 0 0 20px 20px">Edita la comunidad: {{$comunidad->nombre}}.</h6>
		<hr>
    {{ Form::open(array('id' => 'contactform', 'route' => ['comunidades.update',$comunidad],'files' => true, 'method' => 'PUT')) }}

		<div class="row">
			<div class="input-field col s12 m4">
				{!! Form::label('nombre', 'Nombre de la Comunidad') !!}
				{!! Form::text('nombre',$comunidad->nombre,['class' => 'validate',  'required' => 'required']) !!}
			</div>
			<div class="input-field col s12 m4">
				{!! Form::label('municipio', 'Municipio') !!}
				{!! Form::text('municipio',$comunidad->municipio,['class' => 'validate',  'required' => 'required']) !!}
			</div>
			<div class="input-field col s12 m4">
				{!! Form::label('estado', 'Estado') !!}
				{!! Form::text('estado',$comunidad->estado,['class' => 'validate',  'required' => 'required']) !!}
			</div>
		</div>


  	<div class="row">
  		<div class="col s12 center">
  			{!! Form::submit('Actualizar',['id' => 'sub','class' => 'btn blue darken-1', 'style' => 'margin: auto']) !!}
  		</div>
  	</div>


  	{!! Form::close() !!}
	</fieldset>

@endsection
@section('addscripts')
	<script type="text/javascript">
    var frm = $('#contactform');
    frm.submit(function (e) {
        e.preventDefault();
        $.ajax({
            type: frm.attr('method'),
            url: frm.attr('action'),
            data: frm.serialize(),
            success: function (data) {
                console.log(data);
								Materialize.toast(data.nombre +' actualizado.',5000,'teal lighten-2');
								location.reload();
            },
            error: function (data) {
								Materialize.toast('Ocurrió un error en el guardado.',5000,'red');
                console.log(data);
            },
        });
    });
</script>
@endsection
