<ul class="side-nav fixed z-depth-0 collapsible popout" id="slide-out" data-collapsible="accordion">
  <li class="z-depth-0">
    <img src="{{-- asset('img/logos/logo-white.png') --}}" style="height:100%;max-height:50px">
  </li>
  <li class="z-depth-0" style="margin-top:2h0px;color:white">
    <a href="{{ route('comunidades.index') }}" style="color:white"><i class="material-icons" style="color:white">home</i> Comunidades</a>
  </li>
  <li class="divider"></li>
  <li class="z-depth-0" style="margin-top:2h0px;color:white">
    <a href="{{ route('clientesfavs.index') }}" style="color:white"><i class="material-icons" style="color:white">star_rate</i> Clientes Favoritos</a>
  </li>
  <li class="divider"></li>
  <li class="z-depth-0" style="margin-top:2h0px;color:white">
    <a href="{{ route('choferes.index') }}" style="color:white"><i class="material-icons" style="color:white">directions_car</i> Choferes</a>
  </li>
  <li class="divider"></li>
  <li class="z-depth-0" style="margin-top:2h0px;color:white">
    <a href="{{ route('paquetes.index') }}" style="color:white"><i class="material-icons" style="color:white">card_giftcard</i> Paquetes</a>
  </li>
  <li class="divider"></li>
</ul>
