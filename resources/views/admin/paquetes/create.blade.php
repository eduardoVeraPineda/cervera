@extends('admin.templates.layout')
@section('css')

@endsection
@section('title','Crear')

@section('content-fieldset')

	@if(count($errors)>0)

		@foreach($errors->all() as $error)
			<div class="chip red darken-4 white-text">
				{{ $error }}
				<i class="close material-icons">close</i>
			</div>
		@endforeach

	@endif

	<h4>Paquetes</h4>
	<fieldset class="col s12 m12" style="padding: 20px 0px; background-color:white">
		<h6 style="margin: 0 0 20px 20px">Registrar un paquete nuevo.</h6>
		<hr>
		<div id="formulario">

			{{-- {{ Form::open(array('id' => 'form', 'route' => 'paquetes.store','files' => true, 'method' => 'POST')) }} --}}
			<div class="row">
				<div class="input-field col s12 m4">
					<label for="nombre">Nombre</label>
					<input type="text" id="nombre" class="validate" required>
				</div>
				<div class="input-field col s12 m4">
					<label for="apPaterno">A. Paterno</label>
					<input type="text" id="apPaterno" class="validate" required>
				</div>
				<div class="input-field col s12 m4">
					<label for="apMaterno">A. Materno</label>
					<input type="text" id="apMaterno" class="validate" required>
				</div>
			</div>
			<div class="row">
				<div class="input-field col s12 m9">
					<label for="calle">Dirección Completa</label>
					<input type="text" id="calle" class="validate" required>
				</div>
				<div class="col s12 m3">
					{{
						Form::select('comunidad_id',$comunidades,null,['class' => 'browser-default','id' => 'comunidad'])
					}}
				</div>
			</div>
			<div class="row">
				<div class="input-field col s12 m3">
					<label for="telefono">Teléfono</label>
					<input type="number" id="telefono" class="validate" required>
				</div>
				{{--<div class="input-field col s12 m3">--}}
					{{--<label for="pago">Pago</label>--}}
					{{--<input type="number" id="pago" class="validate" required>--}}
				{{--</div>--}}
				<div class="input-field col s12 m3">
					<label for="monto">Monto</label>
					<input type="number" id="monto" class="validate" required>
				</div>
				<div class="input-field col s12 m3">
					<label for="pesoTotal">Peso Total</label>
					<input type="number" id="pesoTotal" class="validate" required>
				</div>
			</div>
			<div class="row">
				<div class="input-field col s12">
					<label for="observaciones">Observaciones</label>
					<textarea id="observaciones" class="validate materialize-textarea" required></textarea>
				</div>
			</div>
			<div id="piezas" class="row">
				<div class="input-field col s12 m3">
					<label for="piezaID" name="codigoBarras">Código</label>
					<input type="text" id="piezaID" class="validate" required>
				</div>
				<div class="input-field col s12 m3">
					<select id="tipo" name="tipo">
						<option value="" disabled selected>Tipo de pieza</option>
						<option value="Bolsa">Bolsa</option>
						<option value="Caja">Caja</option>
						<option value="Amazon">Amazon</option>
						<option value="MK">MK</option>
					</select>
					<label>Selecciona un tipo de pieza</label>
				</div>
				<div class="input-field col s12 m3">
					<label for="peso">Peso (gr)</label>
					<input type="number" id="peso" name="pesoPaquete" class="validate" required>
				</div>
				<div class="input-field col s12 m3">
					<a id="addPieza" class="btn-floating btn-large red darken-1 waves-effect waves-light red"><i class="material-icons">add</i></a>
				</div>
			</div>
			<form id="listaPiezas">

			</form>
			<div class="row">
				<div class="col s12 center">
					<button class="btn blue darken-1" style="margin: auto" id="sub" onclick="validarFormulario()">Registrar</button>
					{{-- {!! Form::submit('Registrar',['id' => 'sub','class' => 'btn blue darken-1', 'style' => 'margin: auto']) !!} --}}
				</div>
			</div>
			{{-- {!! Form::close() !!} --}}

		</div>
	</fieldset>

@endsection
@section('addscripts')
	<script type="text/javascript">

       $("#addPieza").click(function () {
           var codigo = $("#piezaID").val();
           var tipo = $("#tipo").val();
           var peso = $("#peso").val();
		  validarPieza(codigo,tipo,peso);
       });

       function validarPieza(codigo,tipo,peso){
		   //console.log(`CODIGO = ${codigo}, TIPO = ${tipo}, PESO = ${peso}`);
           if(codigo === ""){
			   Materialize.toast("Ingrese el código del la pieza",4000);
			   return;
		   }
		   if(tipo === null){
               Materialize.toast("Ingrese el tipo del la pieza",4000);
               return;
		   }
           if(peso === ""){
               Materialize.toast("Ingrese el peso del la pieza",4000);
               return;
           }

			$("#listaPiezas").append(`
				<div class="row">
					<div class="input-field col s12 m3">
						<input type="text" id="${codigo}" name="${codigo}" value="${codigo}" readonly >
					</div>
					<div class="input-field col s12 m3">
						<input type="text" id="${codigo+tipo}"  name="${codigo}" value="${tipo}" readonly >
					</div>
					<div class="input-field col s12 m3">
						<input type="text" id="${codigo+peso}" name="${codigo}" value="${peso}" readonly >
					</div>
					<div class="input-field col s12 m3">
						<a onclick="quitar('${codigo}')" id="deletePieza" class="btn-floating red darken-1 waves-effect waves-light red"><i class="material-icons">delete</i></a>
					</div>
				</div>

			`);
           var codigo = $("#piezaID").val("");
           var tipo = $("#tipo").val();
           var peso = $("#peso").val("");
       }

       function quitar(id){
           console.log(id);
           $('#'+id).parent().parent().remove();
       }

       function validarFormulario() {
           var nombre = $("#nombre").val();
           var aPaterno = $("#apMaterno").val();
           var aMaterno = $("#apMaterno").val();
           var calle = $("#calle").val();
           var comunidad = $("#comunidad").val();
           var telefono = $("#telefono").val();
//           var pago = $("#pago").val();
           var monto = $("#monto").val();
           var pesoTotal = $("#pesoTotal").val();
					 var observaciones = $("#observaciones").val();
           var paqueteID = $("#paqueteID").val();
					 console.log(paqueteID);

           if(nombre === ""){
               Materialize.toast("Ingresa el nombre",4000);
               return;
				   }
				   if(aPaterno === ""){
		               Materialize.toast("Ingresa el apellido paterno",4000);
		               return;
				   }
				   if(aMaterno === ""){
		               Materialize.toast("Ingresa el apellido materno",4000);
		               return;
				   }
           if(calle === ""){
               Materialize.toast("Ingresa la dirección completa",4000);
               return;
           }
					 if(paqueteID == ""){
               Materialize.toast("Ingresa el ID del paquete",4000);
               return;
					 }
           if(comunidad === undefined){
               Materialize.toast("Ingresa la comunidad",4000);
               return;
           }
           if(telefono === ""){
               Materialize.toast("Ingresa el teléfono",4000);
               return;
           }
//           if(pago === ""){
//               Materialize.toast("Ingresa el pago",4000);
//               return;
//           }
           if(monto === ""){
               Materialize.toast("Ingresa el monto de pago",4000);
               return;
           }
           if(pesoTotal === ""){
               Materialize.toast("Ingresa el peso total",4000);
               return;
           }
           var listado = $('form').serializeArray();

           if(listado.length < 1){
               Materialize.toast("Ingresa una pieza correspondiente al paquete",4000);
               return;
           }
           Materialize.toast("Guardando información, espere un momento.",8000);
           insertarInformacion(listado,nombre,aPaterno,aMaterno,calle,comunidad,telefono,monto,pesoTotal,observaciones);
       }

       function insertarInformacion(listado,nombre,aPaterno,aMaterno,calle,comunidad,telefono,monto,pesoTotal,observaciones){
           var url = "{!! route('paquetes.store') !!}";
           var data = {
               nombre:nombre,
               apPaterno:aPaterno,
               apMaterno:aMaterno,
               telefono:telefono,
               calle:calle,
               pago:1,
               monto:monto,
               pesoTotal:pesoTotal,
			   observaciones:observaciones,
               comunidad_id:comunidad
           };
           var request = $.ajax({
               url: url,
               method: "POST",
               data: JSON.stringify(data),
               contentType: 'application/json',
			     success: function(data) {
                   console.log(data);
                   insertarPiezas(listado,data.id);
               	 },
				 error: function(data,error){
					 console.log('Error: ---------------------');
					 console.log(error);
					 console.log(data);
				 }
           });

       }

       function insertarPiezas(listado,id) {
           let contador = 1;
			for ( var i=0; i < listado.length; i++ ) {
                console.log("--------------");
                var data = {
                    piezaID:listado[i].value,
                    tipo:listado[i+1].value,
                    peso:listado[i+2].value,
					paquete:id
				};
                 console.log(data);
                var url = "{!! route('piezas.store') !!}";
                var request = $.ajax({
                    url: url,
                    method: "POST",
                    data: JSON.stringify(data),
                    contentType: 'application/json',
                    success: function(data) {
                        console.log(data);
                        Materialize.toast("Piezas guardadas "+contador+" de "+(listado.length/3),3000);
                        contador++;

                    },
                    error: function(data,error){
                        console.log('Error: ---------------------');
                        console.log(error);
                        console.log(data);
                        Materialize.toast("Lo sentimos intentelo más tarde.",3000);
                    }
                });
                i+=2;
			}
			console.log(contador + "=" +listado.length );
			if(contador == (listado.length/3)){
                window.location = "/paquetes";
            }
       }
	</script>
@endsection
