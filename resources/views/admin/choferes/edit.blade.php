@extends('admin.templates.layout')
@section('css')

@endsection
@section('title','Crear')

@section('content-fieldset')

	<h4>Choferes</h4>
	<fieldset class="col s12 m12" style="padding: 20px 0px; background-color:white">
		<h6 style="margin: 0 0 20px 20px">Edita la información del chofer.</h6>
		<hr>
    {{ Form::open(array('id' => 'form', 'route' => ['choferes.store',$chofer],'files' => true, 'method' => 'POST')) }}

		<div class="row">
			<div class="input-field col s12 m6">
				{!! Form::label('nombre', 'Nombre completo') !!}
				{!! Form::text('nombre',$chofer->nombre,['class' => 'validate',  'required' => 'required']) !!}
			</div>
			<div class="input-field col s12 m6">
				{!! Form::label('telefono', 'Teléfono') !!}
				{!! Form::text('telefono',$chofer->telefono,['class' => 'validate',  'required' => 'required']) !!}
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12 m12">
				{!! Form::label('direccion', 'Dirección completa') !!}
				{!! Form::text('direccion',$chofer->direccion,['class' => 'validate',  'required' => 'required']) !!}
			</div>
		</div>


  	<div class="row">
  		<div class="col s12 center">
  			{!! Form::submit('Registrar',['id' => 'sub','class' => 'btn blue darken-1', 'style' => 'margin: auto']) !!}
  		</div>
  	</div>


  	{!! Form::close() !!}
	</fieldset>

@endsection
@section('addscripts')
	<script type="text/javascript">
	$('#sub').click(function(){
		var data = $('#form :input').serializeArray();

		$.post($('#form').attr("action"), data, function(status){
			console.log(status);
			Materialize.toast(info.message, 4000,'green');
		});
		clearInput();
		});

	$('#form').submit(function(){
		return false;
	});

	function clearInput(){
		$('#form :input').each(function(){
			$(this).val('');
		});
	}
	</script>
@endsection
