@extends('admin.templates.layout')
@section('css')

@endsection
@section('title','Crear')

@section('content-fieldset')

	<h4>Choferes</h4>
	<fieldset class="col s12 m12" style="padding: 20px 0px; background-color:white">
		<h6 style="margin: 0 0 20px 20px">Dar de alta un nuevo chofer.</h6>
		<hr>
    {{ Form::open(array('id' => 'form', 'route' => 'choferes.store','files' => true, 'method' => 'POST')) }}

		<div class="row">
			<div class="input-field col s12 m6">
				{!! Form::label('nombre', 'Nombre completo') !!}
				{!! Form::text('nombre',null,['class' => 'validate',  'required' => 'required']) !!}
			</div>
			<div class="input-field col s12 m6">
				{!! Form::label('telefono', 'Teléfono') !!}
				{!! Form::text('telefono',null,['class' => 'validate',  'required' => 'required']) !!}
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12 m12">
				{!! Form::label('direccion', 'Dirección completa') !!}
				{!! Form::text('direccion',null,['class' => 'validate',  'required' => 'required']) !!}
			</div>
		</div>


  	<div class="row">
  		<div class="col s12 center">
  			{!! Form::submit('Registrar',['id' => 'sub','class' => 'btn blue darken-1', 'style' => 'margin: auto']) !!}
  		</div>
  	</div>


  	{!! Form::close() !!}
	</fieldset>

@endsection
@section('addscripts')
	<script type="text/javascript">
    var frm = $('#form');
    frm.submit(function (e) {
        e.preventDefault();
        $.ajax({
            type: frm.attr('method'),
            url: frm.attr('action'),
            data: frm.serialize(),
            success: function (data) {
                console.log(data);
								Materialize.toast(data.nombre +' registrado.',5000,'teal lighten-2');
								clearInput();
            },
            error: function (data) {
								Materialize.toast('Ocurrió un error en el guardado.',5000,'red');
                console.log(data);
            },
        });
    });

		function clearInput(){
			$('#form :input').each(function(){
				if($(this).attr('type') == 'submit'){

				}
				else{
					$(this).val('');
				}
			});
		}
</script>
@endsection
