<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// ------------------------------------------ Comunidades
Route::get('/', [
  'uses'  => 'ComunidadController@index',
  'as'    => 'comunidades.index'
]);

Route::get('/comunidades/create', [
  'uses'  => 'ComunidadController@create',
  'as'    => 'comunidades.create'
]);
Route::get('comunidades/{id}/destroy', [
    'uses'	=>	'ComunidadController@destroy',
    'as' 	=>	'comunidades.destroy'
  ]);

// ----------------------------------------- Choferes
Route::get('/choferes', [
  'uses'  => 'ChoferController@index',
  'as'    => 'choferes.index'
]);

Route::get('/choferes/create', [
  'uses'  => 'ChoferController@create',
  'as'    => 'choferes.create'
]);
Route::get('choferes/{id}/destroy', [
    'uses'	=>	'ChoferController@destroy',
    'as' 	=>	'choferes.destroy'
  ]);

// ------------------------------------------ Clientes Favoritos
Route::get('/clientesfavs', [
  'uses'  => 'ClienteController@index',
  'as'    => 'clientesfavs.index'
]);

Route::get('/clientesfavs/create', [
  'uses'  => 'ClienteController@create',
  'as'    => 'clientesfavs.create'
]);
Route::get('clientesfavs/{id}/destroy', [
    'uses'	=>	'ClienteController@destroy',
    'as' 	=>	'clientesfavs.destroy'
  ]);

// ------------------------------------------ Paquetes
Route::get('/paquetes', [
  'uses'  => 'PaqueteController@index',
  'as'    => 'paquetes.index'
]);

Route::get('/paquetes/create', [
  'uses'  => 'PaqueteController@create',
  'as'    => 'paquetes.create'
]);

Route::get('paquetes/{id}/destroy', [
    'uses'	=>	'PaqueteController@destroy',
    'as' 	=>	'paquetes.destroy'
  ]);
