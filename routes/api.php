<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//ALTAS BAJAS Y CAMBIOS DE LOS MÓDULOS
Route::resource('comunidades', 'ComunidadController');
Route::resource('choferes', 'ChoferController');
Route::resource('clientesfavs', 'ClienteController');
Route::resource('paquetes', 'PaqueteController');
Route::resource('piezas', 'PiezaController');
